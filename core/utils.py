#!/usr/bin/python3
import csv
import logging
import requests
import datetime

fb_data_pattern = 'https://graph.facebook.com:443/v2.8/me?fields=albums&access_token={}'


class ProcessCSV(object):
    """
        CSV reader. Organizes reading/formatting of CSV file
    """
    def __init__(self, filepath, delimiter=',', *args, **kwargs):
        """
            :param filepath: absolute path to the file
            :type filepath: str
            :param encoding: file encoding. Default: 'utf-8'
            :type encoding: str
            :param delimiter: file delimiter. Default: ','
            :type delimiter: str
        """
        self.filepath = filepath
        self.delimiter = delimiter
        self.fields = ['fbid', 'token', 'username']

    def is_int(self, elem):
        try:
            return int(elem)
        except ValueError:
            return False

    def get_registration_date(self, result):
        data = requests.get(fb_data_pattern.format(result['token'])).json()

        registration_date = '00-00-00'
        if 'error' in data:
            logging.info(
                'Bad row %s. Message: %s' % (','.join(result.values()), data['error']['message']))
            return False
        for album in data['albums']['data']:
            if album['name'] == 'Profile Pictures':
                dt = datetime.datetime.strptime(
                    album['created_time'], "%Y-%m-%dT%H:%M:%S+%f")
                registration_date = dt.strftime("%Y-%m-%d")
                break
        return registration_date

    def process_row_from_csv(self):
        with open(self.filepath, 'rt') as csvfile:
            reader = csv.reader(csvfile, delimiter=self.delimiter)
            for x, row in enumerate(reader):
                # Skipping empty rows
                if len(row) == 1:
                    continue
                fields = dict(zip(self.fields, row))
                if len(fields.keys()) != 3:
                    logging.info('Not enough data. Line: {}'.format(x))
                    continue
                if not self.is_int(fields.get('fbid', False)):
                    logging.info('"fbid" is absent or not int. Line: {}'.format(x))
                    continue

                result = dict(zip(self.fields, row))

                registration_date = self.get_registration_date(result)
                if not registration_date:
                    continue
                result['registration_date'] = registration_date

                yield result
