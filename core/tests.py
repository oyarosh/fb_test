#!/usr/bin/python3
import unittest
import unittest.mock as mock

from core.utils import ProcessCSV


class ProcessCSVTest(unittest.TestCase):
    def setUp(self):
        p = mock.patch('__main__.open')
        self.mock_file = p.start()

    def test_get_registration_date(self):
        result = {}
        with self.assertRaises(KeyError):
            ProcessCSV('bla.txt').get_registration_date(result)
        result['token'] = 'foo'
        self.assertEqual(ProcessCSV('bla.txt').get_registration_date(result), False)
