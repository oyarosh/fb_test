#!/usr/bin/python3
from __future__ import absolute_import

import os
import sys
import argparse
import logging
import unittest

from core.utils import ProcessCSV
from core.tests import ProcessCSVTest

output_pattern = '{username},{fbid},{registration_date}\r\n'


def main():
    parser = argparse.ArgumentParser(description='''
        The program should calculate the approximate
        date of the user registration in Facebook for each
        record from the csv file.''')
    parser.add_argument('file_name', metavar='file name', type=str, nargs='+',
                        help='file name of csv-data file (ex: fb_text.csv)')
    base_dir = os.path.dirname(os.path.realpath(__file__))
    parser.add_argument('--base_dir', dest='base_dir',
                        default=base_dir,
                        help='''
                            Base directory where file is located.
                            Default: %s''' % base_dir)
    args = parser.parse_args()
    file_path = args.base_dir + '/' + args.file_name[0]
    if not os.path.isfile(file_path):
        sys.stdout.write(
            'File does not exist! file_path {} \r\n'.format(file_path))
        return

    test_run = input("Run test Y/n (default=n)")
    if test_run and test_run == 'Y':
        suite = unittest.TestLoader().loadTestsFromTestCase(ProcessCSVTest)
        unittest.TextTestRunner(verbosity=2).run(suite)

    for row in ProcessCSV(file_path).process_row_from_csv():
        sys.stdout.write(output_pattern.format(
            username=row.get('username'), fbid=row.get('fbid'),
            registration_date=row.get('registration_date')))


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    main()
